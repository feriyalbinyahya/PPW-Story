from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Firriyal Bin Yahya' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 7, 17) #TODO Implement this, format (Year, Month, Date)
npm = 1706979240 # TODO Implement this
hobi = 'Membaca cerita, menonton film, main game, dan mendengarkan musik'
deskripsi = 'Saya seorang mahasiswa yang sedang menuntut ilmu komputer.'
tempatKuliah = 'Universitas Indonesia'
teman_d_name = 'Jonathan Christoper'
teman_d_birth_date = date(1999, 1, 1)
teman_d_npm = 1706040151
teman_b_name = 'Monalisa Valencia'
teman_b_birth_date = date(1999, 12, 11)
teman_b_npm = 1706103543

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'hobby': hobi, 'about_me': deskripsi, 'kuliah': tempatKuliah, 'teman1name': teman_d_name,
				'teman1umur': teman_d_birth_date, 'teman1npm': teman_d_npm, 'teman2name': teman_b_name, 'teman2umur': teman_b_birth_date, 'teman2npm': teman_b_npm}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
